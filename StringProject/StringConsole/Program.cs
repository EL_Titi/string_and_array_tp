﻿using System;

namespace StringConsole
{
    public class Program
    {
        /* returning:
        -0 :starts with a capital, ends with a dot
        -1 :starts with a capital, ends without dot
        -2 :starts without capital, ends with a dot
        -3 :starts without capital, ends without dot */
        public static int StringAnalyse(string string_to_analyse)
        {
            int output = 0, index = -1;

            if (string_to_analyse.Length <= 0)
                return (3);
            while (Char.IsWhiteSpace(string_to_analyse[++index])); //skip whitespaces
            if (!Char.IsUpper(string_to_analyse[index]))
                output += 2;
            index = string_to_analyse.Length;
            while (Char.IsWhiteSpace(string_to_analyse[--index]));
            if (string_to_analyse[index] != '.')
                output += 1;
            return (output);
        }
        static void Main(string[] args)
        {
            string sentence;
            Console.WriteLine("Saisissez une phrase :");
            sentence = Console.ReadLine();
            if (StringAnalyse(sentence) <= 1)
                Console.WriteLine("Votre phrase commence par une majuscule");
            else
                Console.WriteLine("Votre phrase ne commence pas par une majuscule");
            if (StringAnalyse(sentence)%2 == 0)
                Console.WriteLine("Votre phrase se termine par un point");
            else
                Console.WriteLine("Votre phrase ne se termine pas par un point");

        }
    }
}
