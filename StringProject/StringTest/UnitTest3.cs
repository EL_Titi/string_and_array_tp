using System;
using Xunit;

using StringConsole;

namespace StringTest
{
    public class UnitTest3
    {
        /* returning:
        -0 :starts with a capital, ends with a dot
        -1 :starts with a capital, ends without dot
        -2 :starts without capital, ends with a dot
        -3 :starts without capital, ends without dot */
        [Fact]
        public void Test3()
        {
            Assert.Equal(1, Program.StringAnalyse("Bonjour monsieur"));
        }
    }
}
