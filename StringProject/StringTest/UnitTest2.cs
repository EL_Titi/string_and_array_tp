using System;
using Xunit;

using StringConsole;

namespace StringTest
{
    public class UnitTest2
    {
        /* returning:
        -0 :starts with a capital, ends with a dot
        -1 :starts with a capital, ends without dot
        -2 :starts without capital, ends with a dot
        -3 :starts without capital, ends without dot */
        [Fact]
        public void Test2()
        {
            Assert.Equal(2, Program.StringAnalyse("bonjour monsieur."));
        }
    }
}
