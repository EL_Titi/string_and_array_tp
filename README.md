# String_and_array_tp

### About
En reprenant l'énoncé du TP 2 à l'adresse suivante: https://www.delahayeyourself.info/modules/M2104/csharp/tp02_string_and_array/
Adoptez une démarche TDD pour réaliser le développement des deux exercices,
Procédez par étapes en analysant les fonctions que vous aurez besoin d'implémenter pour réaliser le travail demandé,
Créez un dépôt git et pensez à commit à chaque avancée (écriture des tests, implémentation des fonctions, etc.),
Écrivez les tests avant votre code,
Puis lancez-vous dans le développement à proprement dit.
Vous devez donc mettre en oeuvre vos connaissances en algo, TU et TDD, C# et en Git.

### Author
Jason NOEL (jason.noel.imp@gmail.com)