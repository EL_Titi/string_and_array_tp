﻿using System;

namespace ArrayConsole
{
    public class Program
    {
        public static int FindValueInArray(int value_to_find, int[] list)
        {
            int index = -1, output = -1;

            if (list.Length <= 0)
                return (-1);
            while (list[++index] != value_to_find && index + 2 <= list.Length);
            if (value_to_find == list[index])
                output = index + 1;
            return (output);
        }
        static void Main(string[] args)
        {
            string value_string;
            int index, position_of_value = 0, value_to_find = 0;
            int[] list = {1, 1, 1, 1, 1, 1, 1, 1, 1, 1};

            Console.WriteLine("Saisissez 10 valeurs");
            for (index = 0; index < 10; index++)
            {
                Console.Write("{0} : ", index + 1);
                value_string = Console.ReadLine();
                Int32.TryParse(value_string, out list[index]);
            }
            Console.WriteLine("Saisissez une valeur :");
            value_string = Console.ReadLine();
            Int32.TryParse(value_string, out value_to_find);
            position_of_value = FindValueInArray(value_to_find, list);
            if (position_of_value == -1)
                Console.WriteLine("Votre valeur n'est pas dans la liste");
            else
                Console.WriteLine("{0} est la {0} eme valeur saisie.", value_to_find, position_of_value);
            Console.ReadKey();        }
    }
}
