using System;
using Xunit;

using ArrayConsole;

namespace ArrayTest
{
    public class UnitTest5
    {
        [Fact]
        public void Test5()
        {
            int[] list = {4, 7, 34, 7, 88, -9, 0, 51};
            Assert.Equal(-1, Program.FindValueInArray('a', list));
        }
    }
}
