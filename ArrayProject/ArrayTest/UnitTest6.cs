using System;
using Xunit;

using ArrayConsole;

namespace ArrayTest
{
    public class UnitTest6
    {
        [Fact]
        public void Test6()
        {
            int[] list = {4, 7, 34, 7, 88, -9, 0, 51};
            Assert.Equal(2, Program.FindValueInArray(7, list));
        }
    }
}
