using System;
using Xunit;

using ArrayConsole;

namespace ArrayTest
{
    public class UnitTest4
    {
        [Fact]
        public void Test4()
        {
            int[] list = {4, 7, 34, 7, 88, -9, 0, 51};
            Assert.Equal(7, Program.FindValueInArray(0, list));
        }
    }
}
