using System;
using Xunit;

using ArrayConsole;

namespace ArrayTest
{
    public class UnitTest3
    {
        [Fact]
        public void Test3()
        {
            int[] list = {4, 7, 34, 7, 88, -9, 0, 51};
            Assert.Equal(6, Program.FindValueInArray(-9, list));
        }
    }
}
