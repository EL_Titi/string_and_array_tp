using System;
using Xunit;

using ArrayConsole;

namespace ArrayTest
{
    public class UnitTest10
    {
        [Fact]
        public void Test10()
        {
            int[] list = {4};
            Assert.Equal(1, Program.FindValueInArray(4, list));
        }
    }
}
