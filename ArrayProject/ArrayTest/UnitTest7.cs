using System;
using Xunit;

using ArrayConsole;

namespace ArrayTest
{
    public class UnitTest7
    {
        [Fact]
        public void Test7()
        {
            int[] list = {4, 7, 34, 7, 88, -9, 0, 51};
            Assert.Equal(8, Program.FindValueInArray(51, list));
        }
    }
}
