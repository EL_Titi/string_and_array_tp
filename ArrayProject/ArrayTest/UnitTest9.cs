using System;
using Xunit;

using ArrayConsole;

namespace ArrayTest
{
    public class UnitTest9
    {
        [Fact]
        public void Test9()
        {
            int[] list = {4, 7, 34, 7, 88, -9, 0, 51};
            Assert.Equal(2, Program.FindValueInArray(1+2+4, list));
        }
    }
}
