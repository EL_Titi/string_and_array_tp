using System;
using Xunit;

using ArrayConsole;

namespace ArrayTest
{
    public class UnitTest8
    {
        [Fact]
        public void Test8()
        {
            int[] list = {4, 7, 34, 7, 88, -9, 0, 51};
            Assert.Equal(1, Program.FindValueInArray(4, list));
        }
    }
}
